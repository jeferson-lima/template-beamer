# Presentation templates
A list of personal templates for class.


## UTF Template

![](utf_template/images/slide_1.png)

Features:
- Embedded Code - Python style;
- Hide topics

## UTF Template
 
 - WIP

## Simple Template
![](simple/images/slide_1.png)

Features:
- Embedded Code - Python style;
- Hide topics


## Animation Template

![](animation/images/slide_1.png)

Features:
- Animation by csv file;
